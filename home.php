<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package GZ
 */

get_header(); ?>

<!-- Main Banner
============================================= -->
<div id="main-banner" class="owl-carousel main-banner clearfix">

	<div class="item">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner1.jpg" alt="" title="">
		<div class="slider-caption text-right">
			<p style="background: none">Younger looking skin <br><b>is the hallmark</b><br><b> of good health</b></p>
		</div>
	</div>
	
	<div class="item">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner2.jpg" alt="" title="">
		<div class="slider-caption text-right">
			<p style="background: none">What you need  <br><b>is a fresh-faced </b><br><b> glow skin ever</b></p>
		</div>
	</div>
	
	<div class="item">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner3.jpg" alt="" title="">
		<div class="slider-caption text-right">
			<p style="background: none">The beautiful skin  <br><b>requires a commitment, </b><br><b> not a miracle</b></p>
		</div>
	</div>
	
	<div class="item">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner4.jpg" alt="" title="">
		<div class="slider-caption text-right">
			<p style="background: none">Everyone deserves  <br><b>to feel beautiful</b></p>
		</div>
	</div>
	
</div>


<div class="clearfix"></div>
<!-- News and Our Clinic ============================================= -->
<div id="content-index">
	
	<?php get_sidebar('clinicsearch'); ?>

<section class="news-and-our-clinic">
<div class="container">
	<div class="row">
		<div class="row-md-12">
			<h2 class="bordered light">Artikel <span>Terbaru</span></h2>
			
			<div class="col-md-8 blog-wrapper clearfix">
				<?php 
					$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
					if(count($_GET)>0) {

					$args = array(
					    'posts_per_page' => 5, // we need only the latest post, so get that post only
					    //'category_name' => 'article', // Use the category id, can also replace with category_name which uses category slug
					    'category__in' => $_GET['category'],
                        'paged'	=> $paged,
					);
				}else{
					$args = array(
					    'posts_per_page' => 5, // we need only the latest post, so get that post only
					    //'category_name' => 'article', // Use the category id, can also replace with category_name which uses category slug
					    //'category_name' => 'SLUG OF FOO CATEGORY,
                        'paged'	=> $paged,
					);
				}
					// the query
					$the_query = new WP_Query( $args ); ?>

					<?php if ( $the_query->have_posts() ) : ?>

						<!-- pagination here -->

						<!-- the loop -->
						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
							<article class="blog-item blog-full-width">
								<div class="row">
									<div class="col-md-5">
										<div class="blog-thumbnail">
											<img alt="" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>">
											<div class="blog-date"><p class="day"><?php the_time('j') ?></p><p class="monthyear"><?php the_time('M Y') ?></p></div>
										</div>						
									</div>
									<div class="col-md-7">
										<div class="blog-content2">
											<h4 class="blog-title"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h4>
											<p class="blog-meta">By: <a href="#"><?php the_author() ?></a> | <?php the_tags() ?></p>
											<?php the_content('...READ MORE'); ?>
										</div>
									</div>
								</div>
							</article>
						<?php endwhile; ?>
						<!-- end of the loop -->

						<!-- pagination here -->

						<div class="clearfix"></div>
	                        <div class="col-md-12">
	                            <center>
	                                <!-- pagination here -->
									<div class="pagination">
								    <?php
								$big = 999999999; // need an unlikely integer

								echo paginate_links( array(
									'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
									'format' => '?paged=%#%',
									'current' => max( 1, get_query_var('paged') ),
									'total' => $the_query->max_num_pages
								) );
								?>
								</div>
	                            </center>
	                        </div>

                        <?php wp_reset_postdata(); ?>
					<?php else : ?>
						<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
					<?php endif; ?>
			</div>
			
			
			<aside class="col-md-4">
			
			
				
				<?php get_sidebar('filter'); ?>

				
				
				<!-- Recent posts
				============================================= -->
				<!-- <div class="sidebar-widget light">
					
					<h2 class="bordered light">Recent <span>posts</span></h2>
					<?php 
					$args = array(
					    'posts_per_page' => 5, 
					);
					$the_query = new WP_Query( $args ); ?>

					<?php if ( $the_query->have_posts() ) : ?>

						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
							<article class="popular-post">
								<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>" alt="">
								<h4><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h4>
							</article>
						<?php endwhile; ?>
						<?php wp_reset_postdata(); ?>

					<?php else : ?>
						<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
					<?php endif; ?>
				</div> -->

			</aside>
			
		</div>
	</div>
</div>
</section>

</div><!--end #content-index-->
<div class="height20"></div>
		
<!-- <div class="colourfull-row"></div> -->
<div class="solid-row"></div>

<?php
get_footer();
// get_sidebar();