<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package GZ
 */

get_header(); ?>


			
			<!-- Sub Page Banner
			============================================= -->
			<section class="sub-page-banner text-center" data-stellar-background-ratio="0.3">
				
				<div class="overlay"></div>
				
				<div class="container">
					<h1 class="entry-title">Contact Us</h1>
				</div>
				
			</section>
    
    
  
    
			<!-- Sub Page Content
			============================================= -->
			<div id="sub-page-content" class="clearfix">
				
				<div class="container">
					
					<div class="row">
						
						<div class="col-md-4 clearfix">
							
							<h2 class="light bordered">get in <span>touch</span></h2>
							
							
							<!-- Get in Touch Widget
							============================================= -->
							<div class="get-in-touch-widget boxed">
								
								<ul class="list-unstyled">
									<li><i class="fa fa-phone"></i>(+62) - 811859575</li>
									<li><i class="fa fa-envelope"></i><a href="#.">william@myclinicalpro.com</a></li>
									<li><i class="fa fa-globe"></i><a href="#.">www.myclinicalpro.com</a></li>
									<li><i class="fa fa-clock-o"></i>Senin - Jumat, 09.00 - 18.00</li>
									<!-- <li><i class="fa fa-map-marker"></i>Envato Marketplace Melbourne Farlane St Sydney TF - 123456</li>
									<li><i class="fa fa-map-marker"></i>Envato Marketplace Melbourne Farlane St Sydney TF - 123456</li>
 -->								</ul>
								
							</div>
							
							
							
							<!-- Social
							============================================= -->
							<ul class="social-rounded contact clearfix">
								<li><a href="#."><i class="fa fa-facebook"></i></a></li>
								<li><a href="#."><i class="fa fa-twitter"></i></a></li>
								<li><a href="#."><i class="fa fa-google-plus"></i></a></li>
								<li><a href="#."><i class="fa fa-dribbble"></i></a></li>
								<li><a href="#."><i class="fa fa-youtube"></i></a></li>
								<li><a href="#."><i class="fa fa-vimeo-square"></i></a></li>
								<li><a href="#."><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#."><i class="fa fa-rss"></i></a></li>
								<li><a href="#."><i class="fa fa-pinterest"></i></a></li>
								<li><a href="#."><i class="fa fa-android"></i></a></li>
								<li><a href="#."><i class="fa fa-flickr"></i></a></li>
							</ul>
							
							
						</div>
						
						<div class="col-md-8">
							
							<h2 class="light bordered">Give us a <span>Message</span></h2>
							<?php echo do_shortcode( '[contact-form-7 title="Contact us"] ') ?>
							<br>
							<!-- Contact Form
							============================================= -->
							<!-- <div class="contact-form2">
								<div id="message-contact" class="success" style="display:none;">Thank you! we'll contact you shortly.</div>
								<form name="contact_form" id="contact_form" action="submit.php" method="post" onSubmit="return false;">
									<input type="text" name="fname" id="fname" placeholder="First Name" onKeyPress="removeChecks();">
									<input type="text" name="email_address" id="email_address" placeholder="Email Address" onKeyPress="removeChecks();">
									<input type="text" name="subject" id="subject" placeholder="Subject" class="last">
									<textarea placeholder="Message" name="msg" id="msg"></textarea>
									<input type="submit" class="btn btn-default" value="Submit" onClick="validateContact();">
								</form>

							</div> -->
							
						</div>
						
					</div>
					
				</div>
		
		
		
		
		</div><!--end sub-page-content-->
    
    
    
		<div class="colourfull-row"></div>
	

<?php
// get_sidebar();
get_footer();
