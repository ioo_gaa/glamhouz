
<?php if(is_home() || is_category() && (get_post_type( get_the_ID()) != 'clinics' && get_post_type( get_the_ID()) != 'instagram') ) { ?>
	
<!-- Search Widget
				============================================= -->
				<div class="sidebar-widget">
					<div class="search clearfix">
						<form method="get" action="<?php echo get_home_url() ?>">
							<input type="text" name="s" placeholder="Search...">
							<button type="submit" class="search-icon"><i class="fa fa-search"></i></button>
						</form>
					</div>
				</div>
				
				<!-- Categories Widget
				============================================= -->
				<div class="sidebar-widget clearfix">					
					<h2 class="bordered strong">Categories</h2>
					
					<!-- <ul class="tags">
						<?php $categories = get_categories();
						foreach($categories as $category) { ?>
							<li><a href="<?php echo get_category_link($category->term_id) ?>"><?php echo $category->name ?></a></li>
						<?php } ?>
					</ul> -->

					<form action="<?php echo get_home_url() ?>" method="GET">
					<?php 
						$parent_category = (get_categories(array('hierarchical' => true, 'parent' => 0, 'exclude' => 1, 'orderby' => 'term_id')));
						if(!empty($parent_category)) {
							foreach ($parent_category as $key => $parent) {
					?>
					<div class="panel-group" id="accordion<?php echo $key+1 ?>">
					  <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title active">
							<a data-toggle="collapse" data-parent="#accordion<?php echo $key+1 ?>" href="#ac<?php echo $key+1 ?>" class="">
							  <span><i class="fa fa-plus fa-minus"></i></span>
							  <strong><?php echo $parent->name ?></strong>
							</a>
						  </h4>
						</div>

						<div id="ac<?php echo $key+1 ?>" class="panel-collapse in" style="height: auto;">
						  <div class="panel-body">
					<?php
								$child_category = (get_categories(array('hierarchical' => true, 'parent' => $parent->term_id)));
								if(!empty($child_category)) {
									foreach ($child_category as $key => $child) {
										$checked = '';
										if(!empty($_GET['category'])) {
											if(in_array($child->term_id, $_GET['category'])){
													$checked = "checked";
											}
										}
										echo '<div class="checkbox">
												  <label>
												    <input '.$checked.' name="category[]" type="checkbox" value="'.$child->term_id.'">
												    '.$child->name.'
												  </label>
												</div>';
									}
								}
								echo "</div>
									</div>
								  </div>
								</div>";
							}
						}
					?>

					<br>
					<a href="#" class="btn btn-default btn-rounded" style="margin-left: 5px">Reset</a>
					<button type="submit" class="btn btn-default">Filter</button>
				</form>
				</div>
<?php } else { ?>
	
				<!-- sidebar filter -->
				<!-- Categories Widget
				============================================= -->
				
				<div class="sidebar-widget clearfix">
                    <h2 class="bordered strong">Filter Discover</h2>
                    <form action="<?php echo home_url() ?>/index.php/discover" method="get">
                        <div class="radio">
                          <label>
                            <input <?php if(empty($_GET['filter']) || ($_GET['filter'] == 'clinic') || (get_post_type( get_the_ID()) == 'clinics')) echo 'checked' ?> type="radio" value="clinic" name="filter">
                            Aesthetic Clinics
                          </label>
                        </div>

                        <div class="radio">
                          <label>
                            <input <?php if(($_GET['filter'] == 'review') || get_post_type( get_the_ID()) == 'instagram') echo 'checked' ?> type="radio" value="review" name="filter">
                            Aesthetic Review
                          </label>
                        </div>

                        <div class="radio">
                          <label>
                            <input <?php if(!empty($_GET['doctor'])) echo 'checked' ?> type="radio" value="doctor" name="filter" disabled>
                            Doctors
                          </label>
                        </div>
                        <br>
                        <a href="<?php echo get_home_url() ?>/index.php/discover" class="btn btn-default btn-rounded" style="margin-left: 5px">Reset</a>
                        <button type="submit" class="btn btn-default">Filter</button>
                    </form>
				</div>
<?php } ?>