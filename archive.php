<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package GZ
 */

get_header(); ?>

<div class="container">
	<div class="row">
		<div class="row-md-12">
			<?php if((get_post_type( get_the_ID()) == 'clinics' || get_post_type( get_the_ID()) == 'instagram')) : ?>
				<div class="col-md-8">
                <?php if((get_post_type( get_the_ID()) == 'clinics')) { ?>
                    <div class="col-md-12">
                    <h2 class="bordered strong">Aesthetic Clinic</h2>
                    
                    <div id="Container-gallery">
                                
                        <div class="four-column-gallery clearfix" id="four-column-gallery">
                        <?php
                        $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                        $args = array(
                            'posts_per_page' => 10, // we need only the latest post, so get that post only
                            //'category_name' => 'article', // Use the category id, can also replace with category_name which uses category slug
                            //'category_name' => 'SLUG OF FOO CATEGORY,
                            'post_type' => 'clinics',
                            'paged'	=> $paged,
                        );
                        // the query
                        $the_query = new WP_Query( $args ); ?>

                        <?php if ( $the_query->have_posts() ) : ?>

                        <!-- pagination here -->

                        <!-- the loop -->
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                            <div class="col-md-6 mix doctors text-center" style="display: inline-block;">
                                
                                <div class="gallery-item">
                                    <div class="gallery-item-thumb">
                                        <span class="overlay"></span>
                                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>" alt="" title="" width="300px">
                                    </div>
                                    <div class="gallery-item-info">
                                        <p><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></p>
                                        <p style="font-size: 11px"><?php echo get_field('address') ?> , <?php echo get_field('district') ?> , <?php echo get_field('city') ?> , <?php echo get_field('province') ?></p>
                                    </div>
                                </div>
                                
                            </div>
                        <?php endwhile; ?>
                   		</div>
                        <!-- end of the loop -->

                        <!-- pagination here -->
                        </div>   
	                        <div class="clearfix"></div>
	                        <div class="col-md-12">
	                            <center>
	                                <!-- pagination here -->
									<div class="pagination">
								    <?php
								$big = 999999999; // need an unlikely integer

								echo paginate_links( array(
									'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
									'format' => '?paged=%#%',
									'current' => max( 1, get_query_var('paged') ),
									'total' => $the_query->max_num_pages
								) );
								?>
								</div>
	                            </center>
	                        </div>

                        <?php wp_reset_postdata(); ?>

                        <?php else : ?>
                            <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
                        <?php endif; ?>
                                
                    </div>

                    </div>
                <?php } else if(get_post_type( get_the_ID()) == 'instagram') { ?>
                    <div class="col-md-12">
                    <h2 class="bordered strong">Aesthetic Review</h2>
                    
                        <div class="row" id="ms-container">
     
                        <?php                     
                        	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                            $args = array(
                                //'posts_per_page' => 10, // we need only the latest post, so get that post only
                                //'category_name' => 'article', // Use the category id, can also replace with category_name which uses category slug
                                //'category_name' => 'SLUG OF FOO CATEGORY,
                                'post_type' => 'instagram',
                                'paged'	=> $paged,                            );
                            // the query
                            $the_query = new WP_Query( $args ); ?>

                            <?php if ( $the_query->have_posts() ) : ?>

                                <!-- pagination here -->

                                <!-- the loop -->
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        
                                <div class="ms-item col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    
                                        <figure class="article-preview-image">
                                            
                                            <?php the_content() ?>
                                            
                                        </figure>
                                </div>
                        
                                <?php endwhile; ?>

                        </div>   
	                        <div class="clearfix"></div>
	                        <div class="col-md-12">
	                            <center>
	                                <!-- pagination here -->
									<div class="pagination">
								    <?php
								$big = 999999999; // need an unlikely integer

								echo paginate_links( array(
									'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
									'format' => '?paged=%#%',
									'current' => max( 1, get_query_var('paged') ),
									'total' => $the_query->max_num_pages
								) );
								?>
								</div>
	                            </center>
	                        </div>

                				
                           <?php else : ?>

                                <article class="no-posts">

                                    <h1><?php _e('No posts were found.'); ?></h1>

                                </article>
                            <?php endif; ?>
                                     
                    </div>
                <?php } ?>
                </div>
			<?php else : ?>
			<h2 class="bordered light">Archive : <span><?php the_archive_title(); ?></span></h2>
			
			<div class="col-md-8 blog-wrapper clearfix">

					<?php if ( have_posts() ) : ?>

						<!-- pagination here -->

						<!-- the loop -->
						<?php while ( have_posts() ) : the_post(); ?>
							<article class="blog-item blog-full-width" >
								<div class="row">
									<div class="col-md-5">
										<div class="blog-thumbnail">
											<img alt="" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>">
											<div class="blog-date"><p class="day"><?php the_time('j') ?></p><p class="monthyear"><?php the_time('M Y') ?></p></div>
										</div>						
									</div>
									<div class="col-md-7">
										<div class="blog-content2">
											<h4 class="blog-title"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h4>
											<p class="blog-meta">By: <a href="#"><?php the_author() ?></a> | <?php the_tags() ?></p>
											<?php the_excerpt(); ?>
										</div>
									</div>
								</div>
							</article>
						<?php endwhile; ?>
						<!-- end of the loop -->
						
						<!-- pagination here -->
						<?php global $wp_query;
							if ($wp_query->max_num_pages > 1) : ?>
								<nav class="prev-next-posts text-center">
									<span class="prev-posts-link">
										<?php echo get_next_posts_link( 'Older Entries', $the_query->max_num_pages ); ?>
									</span>|
									<span class="next-posts-link">
										<?php echo get_previous_posts_link( 'Newer Entries' ); ?>
									</span>
								</nav>
						<?php endif; ?>
						<?php wp_reset_postdata(); ?>
					<?php else : ?>
						<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
					<?php endif; ?>
			
				<!-- <div class="col-md-12">
					<center>
						<h5><a class="#." href="blog-single-post.html">Load More</a></h5>
					</center>
				</div> -->

			</div>
			
			<?php endif; ?>
			<aside class="col-md-4">
			
			<?php get_sidebar('filter'); ?>
				
			</aside>
			
		</div>
	</div>
</div>

<?php
get_footer();
