<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package GZ
 */

get_header(); ?>
            
            <div class="clearfix"></div>
<!-- News and Our Clinic ============================================= -->
<div id="content-index">
    
    <?php get_sidebar('clinicsearch'); ?>

    <section class="news-and-our-clinic">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                <?php if(empty($_GET['filter']) || $_GET['filter'] == 'clinic' ) { ?>
                    <div class="col-md-12">
                    <h2 class="bordered strong">Aesthetic Clinic</h2>
                    
                    <div id="Container-gallery">
                                
                        <div class="four-column-gallery clearfix" id="four-column-gallery">
                        <?php                     
                        $args = array(
                            'posts_per_page' => 5, // we need only the latest post, so get that post only
                            //'category_name' => 'article', // Use the category id, can also replace with category_name which uses category slug
                            //'category_name' => 'SLUG OF FOO CATEGORY,
                            'post_type' => 'clinics',
                        );
                        // the query
                        $the_query = new WP_Query( $args ); ?>

                        <?php if ( $the_query->have_posts() ) : ?>

                        <!-- pagination here -->

                        <!-- the loop -->
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                            <div class="col-md-6 mix doctors text-center">
                                
                                <div class="gallery-item">
                                    <div class="gallery-item-thumb">
                                        <span class="overlay"></span>
                                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>" alt="" title="" width="300px">
                                    </div>
                                    <div class="gallery-item-info">
                                        <p><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></p>
                                        <p style="font-size: 11px"><?php echo get_field('address') ?> , <?php echo get_field('district') ?> , <?php echo get_field('city') ?> , <?php echo get_field('province') ?></p>
                                    </div>
                                </div>
                                
                            </div>
                        <?php endwhile; ?>
                        <!-- end of the loop -->

                        <!-- pagination here -->

                        <?php wp_reset_postdata(); ?>

                        <?php else : ?>
                            <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
                        <?php endif; ?>
                            
                        </div>
                                
                    </div>
                    
                        <div class="col-md-12">
                            <center>
                                <h5><a class="#." href="<?php echo home_url() ?>/index.php/clinics">Load More</a></h5>
                            </center>
                        </div>

                    </div>
                <?php } else if($_GET['filter'] == 'review' ) { ?>
                    <div class="col-md-12">
                    <h2 class="bordered strong">Aesthetic Review</h2>
                    
                        <div class="row" id="ms-container">
     
                        <?php                     
                            $args = array(
                                'posts_per_page' => 5, // we need only the latest post, so get that post only
                                //'category_name' => 'article', // Use the category id, can also replace with category_name which uses category slug
                                //'category_name' => 'SLUG OF FOO CATEGORY,
                                'post_type' => 'instagram',
                            );
                            // the query
                            $the_query = new WP_Query( $args ); ?>

                            <?php if ( $the_query->have_posts() ) : ?>

                                <!-- pagination here -->

                                <!-- the loop -->
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        
                                <div class="ms-item col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    
                                        <figure class="article-preview-image">
                                            
                                            <?php the_content() ?>
                                            
                                        </figure>
                                </div>
                        
                                <?php endwhile;
                
                            else : ?>

                                <article class="no-posts">

                                    <h1><?php _e('No posts were found.'); ?></h1>

                                </article>
                            <?php endif; ?>
                    
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <center>
                                <h5><a class="#." href="<?php echo home_url() ?>/index.php/instagram">Load More</a></h5>
                            </center>
                        </div>
                    </div>
                <?php } ?>
                </div>
                    
                    <aside class="col-md-4">
                        
                        <?php get_sidebar('filter'); ?>

                    </aside>
                    
                </div>
            </div>
        </section>
            
        </div><!--end sub-page-content-->
    
    
  
        <div class="solid-row"></div>
        

<?php
// get_sidebar();
get_footer();
