<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package GZ
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121831257-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-121831257-1');
	</script>

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
	<?php if(is_page('discover') || is_archive()) { ?>
	    <script type="text/javascript">
        
        jQuery(window).load(function() {
	      var container = document.querySelector('#ms-container');
	      var msnry = new Masonry( container, {
	        itemSelector: '.ms-item',             
	      });  
      
        });

        jQuery(window).load(function() {
	      var container2 = document.querySelector('#four-column-gallery');
	      var msnry = new Masonry( container2, {
	        itemSelector: '.doctors',             
	      });  
      
        });

      
    </script>
	<?php } ?>
</script>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	
<!-- Document Wrapper
		============================================= -->
		<div id="wrapper" class="clearfix fixed-header" >
	<!-- Header
			============================================= -->
			<header id="header" class="medicom-header" <?php if(is_admin_bar_showing()) echo 'style="top: 20px"';  ?>>
			
				<!-- Top Row
				============================================= -->
				<!-- <div class="colourfull-row"></div> -->
				<div class="solid-row"></div>
			
				<div class="container">
					
					
					<!-- Primary Navigation
					============================================= -->
					<nav class="navbar navbar-default" role="navigation">
					
						<!-- Brand and toggle get grouped for better mobile display
						============================================= -->

						<div class="navbar-header">
							
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#primary-nav">
							  <span class="sr-only">Toggle navigation</span>
							  <span class="icon-bar"></span>
							  <span class="icon-bar"></span>
							  <span class="icon-bar"></span>
							</button>
							<?php if(has_header_image()) : ?>
								<a class="navbar-brand" href="<?php echo home_url() ?>"><img src="<?php header_image() ?>" style="margin-bottom: 5px" ></a>
							<?php else : ?>
								<a class="navbar-brand" href="<?php echo home_url() ?>"><h1 style="margin-bottom: 5px" ><?php echo get_bloginfo( 'name' ) ?></h1></a>
							<?php endif ?>
							
							<!-- <a class="navbar-brand" href="index.html"><img src="images/logo.png" alt="" title=""></a> -->
						
						</div>
					
						
						<div class="collapse navbar-collapse navbar-right" id="primary-nav">
							
							<ul class="nav navbar-nav">
								<li class="button_menu">
									<a data-toggle="modal" href="#subscribe-modal" >Subscribe now</a>	
								</li>
								<li class=<?php if(is_home()) : ?>"active" <?php endif ?>>
									<a href="<?php echo home_url() ?>" >Home</a>	
								</li>
								<li class="mega-menu-item dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class=""></i>Skin Care</a>
									<div class="mega-menu dropdown-menu">
										<div class="row container">
											<!-- <img src="images/mega-menu-img.jpg" class="img-rounded" alt="" title=""> -->
												<?php 
													$parent_category = (get_categories(array('hierarchical' => true, 'parent' => 0, 'exclude' => 1, 'orderby' => 'term_id')));
													if(!empty($parent_category)) {
														foreach ($parent_category as $key => $parent) {
												?>
														<ul>
															<li><strong><?php echo $parent->name ?></strong></li>
												<?php  
															$child_category = (get_categories(array('hierarchical' => true, 'parent' => $parent->term_id)));
															if(!empty($child_category)) {
																foreach ($child_category as $key => $child) {
											
																echo "<li><a href='".get_category_link( $child->term_id )."'>" .$child->name. "</a></li>";
												
																}
															}
															echo "</ul>";
														}
													}
												?>
											
										</div>
										<div class="row container">
											<br>
											<ul class="tags" >
												<li><a href="#" style="padding: 5px 5px 5px 5px; border-color: #2B96CC;">Semua Topik</a></li>
											</ul>
										</div>
									</div>
								</li>
								<li class=<?php if(is_page('about')) : ?>"active" <?php endif ?>>
									<a href="<?php echo home_url() ?>/index.php/discover" >Discover</a>	
								</li>
								<li class=<?php if(is_page('contact')) : ?>"active" <?php endif ?>>
									<a href="<?php echo home_url() ?>/index.php/contact" >Contact</a>	
								</li>
								<!-- <li class="dropdown">
									<a href="#">Booking</a>	
								</li>
								<li class="dropdown">
									<a href="#">Shop</a>	
								</li> -->

								<?php if(!is_user_logged_in()) : ?>
									<li class="dropdown <?php if(is_page('login')) : ?>active<?php endif ?>">
										<a href="<?php echo home_url() ?>/index.php/login" >Login / Register</a>	
									</li>
								<?php else : ?>
									<?php $current_user = wp_get_current_user(); ?>
									<li class="dropdown last">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $current_user->user_login; ?> <i class="fa fa-angle-down"></i></a>
										<ul class="dropdown-menu">      
											<li><a href="<?php echo home_url() ?>/index.php/profile"><i class="fa fa-user"></i> Profil </a></li>
											<li><a href="<?php echo home_url('index.php'); ?>/index.php/change-password"><i class="fa fa-lock"></i> Ubah kata sandi </a></li>
											<li><a href="<?php echo wp_logout_url('index.php'); ?>"><i class="fa fa-sign-out"></i> Keluar</a></li>
										</ul>
									</li>
								<?php endif ?>
							</ul>
							
						</div>
						
					</nav>

				</div>
				<div class="header-bottom-line"></div>
			</header>

	<div id="content" class="site-content">
