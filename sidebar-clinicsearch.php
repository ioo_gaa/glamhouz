<?php 
$args = array(
    'post_type' => 'clinics',
);
// the query
$the_query = new WP_Query( $args ); ?>

<section class="features bg-doodle">
					
		<div class="container">
		
			<div class="row">
				
				<div class="col-md-12 text-center">
				
					<div class="feature">
						<div class="feature-content">
						<i class="feature-icon fa fa-stethoscope" style="margin-bottom: 10px;"></i>
							<h4 style="color:#FFF">Find Your Favourite Aesthetic Clinics</h4>
							<div class="col-md-12">
							<form action="<?php echo home_url() ?>/index.php/discover">
							    <div class="row">
							        <div class="col-xs-4">
							            <div class="input-group">
							                <span class="input-group-addon"><span class="fa fa-plus-square"></span></span>
							                <select class="form-control" name="cname">
							                	<option value="">- Jenis Klinik -</option>
							                	<option value="">1</option>
							                	<option>1</option>
							                </select>	
							            </div>
							        </div>
							        <div class="col-xs-4">
							            <div class="input-group">
							                <span class="input-group-addon"><span class="fa fa-map-marker"></span></span>
							                <select class="form-control" name="loc">
							                	<option value="">- Lokasi -</option>
												<?php if ( $the_query->have_posts() ) : ?>
												<!-- the loop -->
												<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
													<option value=""><?php echo get_field('district') ?> , <?php echo get_field('city') ?> , <?php echo get_field('province') ?></option>
												<?php endwhile; 
													endif;
												?>
							                </select>
							            </div>
							        </div>
							        <div class="col-xs-4">
							            <div class="input-group">
							                <input type="submit" class="form-control" value="search">
							            </div>
							        </div>
							    </div>
							</form>
							</div>
						</div>
					</div>
				
				</div>
			</div>			
		</div>
		
	</section>