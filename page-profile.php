<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package GZ
 */

get_header(); ?>

<div class="clearfix"></div>
<div class="height20"></div>
    
	<!-- Sub Page Banner
		============================================= -->
	<section class="sub-page-banner text-center" data-stellar-background-ratio="0.3">
		
		<div class="overlay"></div>
		
		<div class="container">
			<h1 class="entry-title">Member Detail</h1>
		</div>
		
	</section>

	<div id="sub-page-content" class="clearfix">
		<section class="team-description">
			<div class="container">
				<ul id="meet-specialist-carousel" class="list-unstyled owl-carousel row">
					<li>
						<div class="doctors-img"><?php global $userdata; echo get_avatar($userdata->ID, 531); ?>
						</div>
						<div class="doctors-detail">
							<?php $current_user = wp_get_current_user(); ?>
							<?php if($current_user->user_firstname === '') : ?>
								<h4><?php echo $current_user->user_login ?></h4>
							<?php else : ?>
								<h4><?php echo $current_user->user_firstname; ?> <?php echo $current_user->user_lastname; ?><span><?php echo $current_user->user_login ?></span></h4>
							<?php endif ?>
							<p><label class="heading">Email</label><label class="detail"><?php echo $current_user->user_email ?></label></p>
							<p><label class="heading">No. Telepon</label><label class="detail"> <?php echo $current_user->telp ?> </label></p>
							<p><label class="heading">Gender</label><label class="detail"> <?php echo $current_user->gender ?> </label></p>
							<p><label class="heading">Tanggal Lahir</label><label class="detail"> <?php echo $current_user->birth_date ?> </label></p>
							<p><label class="heading">Alamat</label><label class="detail"> <?php echo $current_user->address ?> </label></p>
							<div class="height20"></div>
							<a href="<?php echo home_url() ?>/index.php/update-profile" class="btn btn-default btn-mini btn-rounded">Ubah Profil</a>
						</div>
					</li>
				</ul>
			</div>
		</section>
	</div><!--end sub-page-content-->
<?php
get_footer();
