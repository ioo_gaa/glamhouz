<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package GZ
 */

get_header(); ?>

	<!-- <div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		// while ( have_posts() ) : the_post();

			// get_template_part( 'template-parts/content', get_post_type() );

			// the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			// if ( comments_open() || get_comments_number() ) :
				// comments_template();
			// endif;

		// endwhile; 
		// End of the loop.
		?>

		</main> -->
		<!-- #main -->
	<!-- </div> -->
	<!-- #primary -->

	<!-- Sub Page Banner
		============================================= -->
		<section class="sub-page-banner2 text-center" data-stellar-background-ratio="0.3">
			
			<div class="overlay"></div>
			
			<div class="container">
				<h1 class="entry-title">Blog Detail</h1>
			</div>
			
		</section>

		<!-- Sub Page Content
		============================================= -->
		<div id="sub-page-content" class="clearfix">
    	
			<div class="container">
				
				<div class="row">
				
					<div class="col-md-8 blog-wrapper clearfix">
						<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<h2 class="bordered light"><?php the_title() ?></h2>
						
						<article class="blog-item blog-full-width blog-detail" <?php post_class() ?> id="post-<?php the_ID(); ?>">
							
							<div class="blog-thumbnail" width="300px">
								<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>" alt="" title="" width="">
							</div>
							
							<div class="blog-content">
								<?php the_content(); ?>                   
							</div>
										
										
							<div class="share-post clearfix">
								<label>Share this Post!</label>
								<ul class="social-rounded">
									<li><a href="#."><i class="fa fa-facebook"></i></a></li>
									<li><a href="#."><i class="fa fa-twitter"></i></a></li>
									<li><a href="#."><i class="fa fa-google-plus"></i></a></li>
									<li><a href="#."><i class="fa fa-dribbble"></i></a></li>
									<li><a href="#."><i class="fa fa-youtube"></i></a></li>
									<li><a href="#."><i class="fa fa-vimeo-square"></i></a></li>
									<li><a href="#."><i class="fa fa-linkedin"></i></a></li>
								</ul>
							</div>
										
						</article>
						
						<?php 
							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
						 ?>
						
					</div>
					<?php endwhile; endif; ?>
					
					<aside class="col-md-4">
					
					<?php get_sidebar('filter'); ?>
						
					</aside>
					
				</div>
				
			</div>



	</div><!--end sub-page-content-->



	<div class="solid-row"></div>
	

<?php
get_footer();
