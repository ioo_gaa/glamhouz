<?php
/**
 * The template for displaying search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package GZ
 */

get_header(); ?>
<section class="sub-page-banner2 text-center" data-stellar-background-ratio="0.3" style="background-position: 50% 24.3px;">
			
			<div class="overlay"></div>
			
			<div class="container">
				<h1 class="entry-title">Search Result</h1>
			</div>
			
		</section>
<div class="container">
	<div class="row">
		<div class="row-md-12">
			<h2 class="bordered light">Keyword :<strong> "<?php echo get_search_query() ?>"</strong></h2>
			
			<div class="col-md-8 blog-wrapper clearfix">
				<?php 
					$paged =  get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
					$args = array(
					  'posts_per_page' => 5,
					  'paged'          => $paged,
					  's' => get_search_query(),
				      'post_type' => 'post', 
				      'ignore_sticky_posts' => true,
					  'post_status' => 'publish',
					);
					// the query
					$the_query = new WP_Query( $args ); ?>

					<?php if ( $the_query->have_posts() ) : ?>

						<!-- pagination here -->

						<!-- the loop -->
						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
							<article class="blog-item blog-full-width">
								<div class="row">
									<div class="col-md-5">
										<div class="blog-thumbnail">
											<img alt="" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>">
											<div class="blog-date"><p class="day"><?php the_time('j') ?></p><p class="monthyear"><?php the_time('M Y') ?></p></div>
										</div>						
									</div>
									<div class="col-md-7">
										<div class="blog-content2">
											<h4 class="blog-title"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h4>
											<p class="blog-meta">By: <a href="#"><?php the_author() ?></a> | <?php the_tags() ?></p>
											<?php the_excerpt(); ?>
										</div>
									</div>
								</div>
							</article>
						<?php endwhile; ?>
						<!-- end of the loop -->
						<?php wp_reset_postdata(); ?>
						<?php if ($the_query->max_num_pages > 1) : // custom pagination  ?>
							<?php
							$orig_query = $wp_query; // fix for pagination to work
							$wp_query = $the_query;
							?>
							<nav class="prev-next-posts text-center">
								<span class="prev-posts-link">
									<?php echo get_next_posts_link( 'Older Entries', $the_query->max_num_pages ); ?>
								</span>|
								<span class="next-posts-link">
									<?php echo get_previous_posts_link( 'Newer Entries' ); ?>
								</span>
							</nav>
							<?php
							$wp_query = $orig_query; // fix for pagination to work
							?>
						<?php endif; ?>
						<!-- pagination here -->

					<?php else : ?>
						<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
					<?php endif; ?>
			
				<!-- <div class="col-md-12">
					<center>
						<h5><a class="#." href="blog-single-post.html">Load More</a></h5>
					</center>
				</div> -->

			</div>
			
			
			<aside class="col-md-4">
			
			<?php get_sidebar('filter'); ?>

			</aside>
			
		</div>
	</div>
</div>

<?php
get_footer();

