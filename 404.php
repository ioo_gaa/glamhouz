<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package GZ
 */

get_header(); ?>

	<!-- Sub Page Banner
			============================================= -->
			<section class="sub-page-banner text-center" data-stellar-background-ratio="0.3">
				
				<div class="overlay"></div>
				
				<div class="container">
					<h1 class="entry-title">404 Error</h1>
					<!-- <p>PAGE NOT FOUND.</p> -->
				</div>
				
			</section>
    
    
			
			<!-- Sub Page Content
			============================================= -->
			<div id="sub-page-content" class="clearfix">

				<div class="container">
					
					<div class="error-text">
					
						<p><span><strong>404</strong> Something went wrong here!</span>
						Oops! Sorry, an error has occured. Requested page not found!</p>
						<a href="<?php echo home_url() ?>" class="btn btn-default btn-rounded">go back to home</a>
					
					</div>
					
				</div>
     
			</div><!--end sub-page-content-->
    

    
		<div class="colourfull-row"></div>
		
		

<?php
get_footer();
