<?php 

function extra_user_profile_fields( $user ) { ?>
    <h3><?php _e("Extra profile information", "blank"); ?></h3>

    <table class="form-table">
    <tr>
        <th><label for="telp"><?php _e("No. Telphone"); ?></label></th>
        <td>
            <input type="text" name="telp" id="telp" value="<?php echo esc_attr( get_the_author_meta( 'telp', $user->ID ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter your telephone number."); ?></span>
        </td>
    </tr>
    <tr>
        <th><label for="gender"><?php _e("Gender"); ?></label></th>
        <td>
        	<select name="gender" class="form-control">
			  <option> select </option>
			  <option <?php if( get_the_author_meta( 'gender', $user->ID )=='male' ) : ?> selected="selected" <?php endif ?> value="male">Male</option>
			  <option <?php if( get_the_author_meta( 'gender', $user->ID )=='female' ) : ?> selected="selected" <?php endif ?> value="female">Female</option>
			</select><br>
            <!-- <input type="text" name="gender" id="gender" value="<?php echo esc_attr( get_the_author_meta( 'gender', $user->ID ) ); ?>" class="regular-text" /><br /> -->
            <span class="description"><?php _e("Please select your gender."); ?></span>
        </td>
    </tr>
    <tr>
        <th><label for="telp"><?php _e("Birth date"); ?></label></th>
        <td>
            <input type="text" name="birth_date" id="birth_date" value="<?php echo esc_attr( get_the_author_meta( 'birth_date', $user->ID ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter your birth date."); ?></span>
        </td>
    </tr>
    <tr>
        <th><label for="address"><?php _e("Address"); ?></label></th>
        <td>
            <textarea type="text" name="address" id="address" class="regular-text" /><?php echo esc_attr( get_the_author_meta( 'address', $user->ID ) ); ?></textarea><br>
            <span class="description"><?php _e("Please enter your address."); ?></span>
        </td>
    </tr>
    </table>
<?php }

add_action( 'show_user_profile', 'extra_user_profile_fields' );
add_action( 'edit_user_profile', 'extra_user_profile_fields' );

function save_extra_user_profile_fields( $user_id ) {
    if ( !current_user_can( 'edit_user', $user_id ) ) { 
        return false; 
    }
    update_user_meta( $user_id, 'address', $_POST['address'] );
    update_user_meta( $user_id, 'telp', $_POST['telp'] );
    update_user_meta( $user_id, 'birth_date', $_POST['birth_date'] );
    update_user_meta( $user_id, 'gender', $_POST['gender'] );
}

add_action( 'personal_options_update', 'save_extra_user_profile_fields' );
add_action( 'edit_user_profile_update', 'save_extra_user_profile_fields' );

