<?php
/*
Template Name: Custom edit profile
*/
global $wpdb, $user_ID, $result;
function handle_form_submission() {
	// do your validation, nonce and stuffs here

	$current_user = wp_get_current_user();
	// Instantiate the WP_Error object
	$my_error = new WP_Error();

	// Check username is present and not already in use
	$name = $_POST['contact-name'];
	if(empty($name)) { 
		$my_error->add( 'contact-name', 'Name is required.', $name );
	}

	// Check email address is present and valid
	$email = $_POST['contact-email'];
	if( empty( $email ) ) {
		$my_error->add( 'contact-email', 'Email is required.', $email );
	} elseif( !is_email( $email ) ) { 
		$my_error->add( 'contact-email', 'Please enter valid email.', $email );
	} elseif( email_exists( $email ) && $email != $current_user->user_email) {
		$my_error->add( 'contact-email', "This email address is already in use.", $email );
	}

	$telp = $_POST['telp'];
	if(!empty($telp) && !is_numeric($telp)){
		$my_error->add( 'contact-telp', "Invalid telp number.", $telp );	
	}

	// Send the result
	if ( !empty( $my_error->get_error_codes() ) ) {
		return $my_error;
	}

	// Everything is fine
	return true;
}

if( $_SERVER['REQUEST_METHOD'] == 'POST' ) {

	$current_user = wp_get_current_user();
	$result = handle_form_submission();
	if ( !is_wp_error( $result ) ) {
		$user_id = $current_user->ID;
		wp_update_user( array( 
			'ID' => $user_id, 
			'first_name' => $_POST['contact-name'],
			'email'		=> $_POST['email']
			) 
		);
		update_user_meta( $user_id, 'telp', $_POST['contact-telp'] );
		update_user_meta( $user_id, 'birth_date', $_POST['contact-birthdate'] );
		update_user_meta( $user_id, 'address', $_POST['contact-address'] );
		update_user_meta( $user_id, 'gender', $_POST['contact-gender'] );

		$page  = get_permalink( get_page_by_title('update-profile') );
		wp_redirect( $page.'/?success=1' );
	}

}
get_header(); ?>

<div class="clearfix"></div>
<div class="height20"></div>
    
	<!-- Sub Page Banner
		============================================= -->
	<section class="sub-page-banner text-center" data-stellar-background-ratio="0.3">
		
		<div class="overlay"></div>
		
		<div class="container">
			<h1 class="entry-title">Update Member Detail</h1>
			<p>Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets.</p>
		</div>
		
	</section>

	<div id="sub-page-content" class="clearfix">
		<section class="team-description">
			<div class="container">
				<ul id="meet-specialist-carousel" class="list-unstyled owl-carousel row">
					<li>
						<div class="doctors-img">
							<?php global $userdata; echo get_avatar($userdata->ID, 531); ?>
							<?php $current_user = wp_get_current_user(); ?>
						</div>
						<div class="doctors-detail">
							<?php if($current_user->user_firstname === '') : ?>
								<h4><?php echo $current_user->user_login ?></h4>
							<?php else : ?>
								<h4><?php echo $current_user->user_firstname; ?> <?php echo $current_user->user_lastname; ?><span><?php echo $current_user->user_login ?></span></h4>
							<?php endif ?>
							<form action="<?php the_permalink() ?>" method="POST">
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-3">Nama</label>
										<div class="col-md-9">
											<input type="text" name="contact-name" class="form-control" required="" value="<?php echo $current_user->first_name ?>">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3">Email</label>
										<div class="col-md-9">
											<input type="text" name="contact-email" class="form-control" required="" value="<?php echo $current_user->user_email ?>">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3">No. Telp</label>
										<div class="col-md-9">
											<input type="text" name="contact-telp" class="form-control" value="<?php echo $current_user->telp ?>">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3">Gender</label>
										<div class="col-md-9">
											<select class="form-control" name="contact-gender">
												<option> Pilih </option>
												<option value="male" <?php if($current_user->gender == 'male') echo 'selected="selected" '; ?>> Laki-laki </option>
												<option value="female" <?php if($current_user->gender == 'female') echo 'selected="selected" '; ?>> Perempuan </option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3">Tanggal Lahir</label>
										<div class="col-md-9">
											<input type="text" name="contact-birthdate" class="form-control datepicker" data-date-format="mm/dd/yyyy" value="<?php echo $current_user->birth_date ?>">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-3">Alamat</label>
										<div class="col-md-9">
											<textarea class="form-control" name="contact-address"><?php echo $current_user->address ?></textarea>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-default" type="submit">Update</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</li>
				</ul>
			</div>
		</section>
	</div><!--end sub-page-content-->
<?php
get_footer();
