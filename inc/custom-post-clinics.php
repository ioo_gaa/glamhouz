<?php
// Register Custom Post Types
add_action('init', 'register_clinics_init');
function register_clinics_init() {
    // Register Products
    $products_labels = array(
        'name'               => 'Clinics',
        'singular_name'      => 'Clinic',
        'menu_name'          => 'Clinics'
    );
    $products_args = array(
        'labels'             => $products_labels,
        'public'             => true,
        'capability_type'    => 'post',
        'has_archive'        => 'clinics',
        'supports'           => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions' ),
        // 'rewrite' => array( 'slug' => 'clin', 'with_front' => true ),
    );
    register_post_type('clinics', $products_args);
    flush_rewrite_rules();
}

// Register Custom Post Types
add_action('init', 'register_instagram_init');
function register_instagram_init() {
    // Register Products
    $products_labels = array(
        'name'               => 'Instagrams',
        'singular_name'      => 'Instagram',
        'menu_name'          => 'Instagram'
    );
    $products_args = array(
        'labels'             => $products_labels,
        'public'             => true,
        'capability_type'    => 'post',
        'has_archive'        => 'instagram',
        'supports'           => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions' ),
        // 'rewrite' => array( 'slug' => 'inst', 'with_front' => true ),
    );
    register_post_type('instagram', $products_args);
    flush_rewrite_rules();
}
