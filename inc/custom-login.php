<?php 

/*
Template Name: Login & Register
*/

/** Register function **/
require_once(ABSPATH . WPINC . '/registration.php');
global $wpdb, $user_ID, $result;

function handle_form_submission() {
	// do your validation, nonce and stuffs here

	// Instantiate the WP_Error object
	$my_error = new WP_Error();

	// Check username is present and not already in use
	$name = $_POST['contact-name'];
	if(empty($name)) { 
		$my_error->add( 'contact-name', 'Name is required.', $name );
	}

	// Check email address is present and valid
	$email = $_POST['contact-email'];
	if( empty( $email ) ) {
		$my_error->add( 'contact-email', 'Email is required.', $email );
	} elseif( !is_email( $email ) ) { 
		$my_error->add( 'contact-email', 'Please enter valid email.', $email );
	} elseif( email_exists( $email ) ) {
		$my_error->add( 'contact-email', "This email address is already in use.", $email );
	}

	$telp = $_POST['telp'];
	if(!empty($telp) && !is_numeric($telp)){
		$my_error->add( 'contact-telp', "Invalid telp number.", $telp );	
	}

	// Check password is valid
	if(0 === preg_match("/.{6,}/", $_POST['password'])){
		$my_error->add( 'invalid', "Password must be at least six characters." );
	}

	// Check password confirmation_matches
	if(0 !== strcmp($_POST['password'], $_POST['password_confirmation'])){
	  	$my_error->add( 'invalid', "Password does not match." );	
	}

	// Send the result
	if ( !empty( $my_error->get_error_codes() ) ) {
		return $my_error;
	}

	// Everything is fine
	return true;
}

function generate_unique_username( $username ) {
	static $i;
	if ( null === $i ) {
		$i = 1;
	} else {
		$i++;
	}
	$username = str_replace(' ', '', $username);
	if ( ! username_exists( $username ) ) {
		return $username;
	}
	$new_username = sprintf( '%s-%s', $username, $i );
	if ( ! username_exists( $new_username ) ) {
		return $new_username;
	} else {
		return call_user_func( __FUNCTION__, $username );
	}
}

if( $_SERVER['REQUEST_METHOD'] == 'POST' ) {

	$result = handle_form_submission();
	// print_r(is_wp_error( $result ));exit();
	if ( !is_wp_error( $result ) ) {
		// Everything went well
		$fullname = $_POST['contact-name'];
		$username = generate_unique_username($fullname);
		$password = $_POST['password'];
		$email = $_POST['contact-email'];
		$telp = $_POST['telp'];

		$new_user_id = wp_create_user( $username, $password, $email );

		// You could do all manner of other things here like send an email to the user, etc. I leave that to you.
		// will return false if the previous value is the same as $new_value
		wp_update_user( array( 'ID' => $new_user_id, 'first_name' => $fullname ) );
		update_user_meta( $new_user_id, 'telp', $telp );

		$success = 1;
		$login_page  = get_permalink( get_page_by_title('login') );
		wp_redirect( $login_page.'/?regsuccess=1' );
		// header( 'Location:' . get_bloginfo('url') . 'index.php/login/?success=1&u=' . $username );
	}

}

/** End register function **/
?>


<?php
/** redirect login custom **/
function redirect_login_page(){
	
    // Store for checking if this page equals wp-login.php
    $page_viewed = basename( $_SERVER['REQUEST_URI'] );

    // permalink to the custom login page
    $login_page  = get_permalink( get_page_by_title('login') );

    if( $page_viewed == "wp-login.php"  && $_SERVER['REQUEST_METHOD'] == 'GET') {
        wp_redirect( $login_page );
        exit();
    }
}

add_action( 'init','redirect_login_page' );


function login_failed() {
	$login_page  = get_permalink( get_page_by_title('login') );
	wp_redirect( $login_page . '/?login=failed' );
	exit;
}
add_action( 'wp_login_failed', 'login_failed' );

function blank_username_password( $user, $username, $password ) {
	$login_page  = get_permalink( get_page_by_title('login') );
	if( $username == "" || $password == "" ) {
	wp_redirect( $login_page . "/?login=blank" );
	exit;
}
}
add_filter( 'authenticate', 'blank_username_password', 1, 3);

//echo $login_page = $page_path ;

function logout_page() {
	$login_page  = get_permalink( get_page_by_title('login') );
	wp_redirect( $login_page . "/?login=false" );
	exit;
}
add_action('wp_logout', 'logout_page');

?>

<?php get_header(); ?>

<div class="clearfix"></div>
<div class="height20"></div>
    
	<?php if(!is_user_logged_in()) : ?>
		<?php $page_showing = basename($_SERVER['REQUEST_URI']); ?>
		<div id="content-index">
			<!-- login ============================================= -->
			<div class="cont <?php if(is_wp_error($result)) : ?> s--signup <?php endif ?>">
			  <div class="formlogin sign-in">
			    <h2>Welcome</h2>
			    <div class="appointment-form clearfix" style="padding-left: 25px;padding-right: 25px;">
				   <div class="success" id="message-app" style="display:none;"></div>
				   <?php 
				   	if (strpos($page_showing, 'regsuccess') !== false) {
						echo '<div class="alert alert-success text-center"><p class="text-success"><strong>Success:</strong> Registrasion user success.</p></div>';
					}
					if (strpos($page_showing, 'failed') !== false) {
						echo '<div class="alert alert-danger text-center"><p class="text-danger"><strong>ERROR:</strong> Invalid username and/or password.</p></div>';
					}
					elseif (strpos($page_showing, 'blank') !== false ) {
						echo '<div class="alert alert-danger text-center"><p class="text-danger"><strong>ERROR:</strong> Username and/or Password is empty.</p></div>';
					} ?>
				   <form method="post" action="<?php bloginfo('url') ?>/wp-login.php" class="wp-user-form">
						<div class="row">
                            <div class="col-md-6">
								<label for="user_login"><?php _e('Email'); ?>: </label>
								<input type="text" name="log" value="<?php echo esc_attr(stripslashes($user_login)); ?>" id="user_login" tabindex="11" />
							</div>
                            <div class="col-md-6">
								<label for="user_pass"><?php _e('Password'); ?>: </label>
								<input type="password" name="pwd" value="" id="user_pass" tabindex="12" />
							</div>
						</div>
						<!-- <div class="login_fields">
							<div class="rememberme">
								<label for="rememberme">
									<input type="checkbox" name="rememberme" value="forever" checked="checked" id="rememberme" tabindex="13" /> Remember me
								</label>
							</div>
						</div> -->
						<div class="row">
                            <div class="col-sm-6 pull-right">
                                <div class="forgot-password">
                                	<!-- <a data-toggle="modal" href="#forgot" id="forgot" data-target="#forgot-modal">Forgot Password?</a>&nbsp; &nbsp; -->
									<?php do_action('login_form'); ?>
									<input type="submit" name="user-submit" value="<?php _e('Login'); ?>" class="user-submit btn btn-default btn-rounded" />
									<input type="hidden" name="redirect_to" value="<?php echo $_SERVER['REQUEST_URI']; ?>" />
									<input type="hidden" name="user-cookie" value="1" />
								</div>							
							</div>
						</div>
					</form>
				</div>
			  </div>
			  <div class="sub-cont">
			    <div class="img">
			      <div class="img__text m--up">
			        <h2 class="white">New here?</h2>
			        <p>Sign up and discover great amount of new opportunities!</p>
			      </div>
			      <div class="img__text m--in">
			        <h2 class="white">One of us?</h2>
			        <p>If you already has an account, just sign in. We've missed you!</p>
			      </div>
			      <div class="img__btn">
			        <span class="m--up">Sign Up</span>
			        <span class="m--in">Sign In</span>
			      </div>
			    </div>
			    <div class="form sign-up">
			    	<h2>Welcome</h2>
				   <div class="success" id="message-app" style="display:none;"></div>
			        <form class="form-horizontal" role="form" action="<?php the_permalink() ?>" method="POST">
			        	<?php wp_nonce_field( 'submit_reg', 'custom_reg' ); ?>
                        <div class="form-body">
                            <?php if ( is_wp_error( $result ) ) : ?>
                            <div class="form-group">
                            	<div class="col-md-offset-3 col-md-9 alert alert-danger" style="padding-left: 25px">
									<p class="text-danger">Ops, something went wrong. Please rectify this errors</p>
									<ul>
										<?php echo '<li>' . implode( '</li><li>', $result->get_error_messages() ) . '</li>'; ?>
									</ul>
								</div>
                            </div>
							<?php endif ?>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Nama</label>
                                <div class="col-md-9">
                                    <input type="text" name="contact-name" class="form-control input-inline input-medium" placeholder="" value="" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Email</label>
                                <div class="col-md-9">
                                    <input type="email" name="contact-email" class="form-control" required="" placeholder="Email Address"> </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Password</label>
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="password" name="password" class="form-control input-inline input-medium" placeholder="Password" required=""  minlength="6">
                                            <!-- /input-group -->
                                        </div>
                                        <!-- /.col-md-6 -->
                                        <div class="col-md-6">
                                            <input type="password" name="password_confirmation" class="form-control input-inline input-medium" placeholder="Re-type Password" required=""  minlength="6">
                                            <!-- /input-group -->
                                        </div>
                                        <!-- /.col-md-6 -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">No Telepon</label>
                                <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" name="telp" class="form-control input-inline input-medium" placeholder="">
                                                <!-- /input-group -->
                                            </div>
                                            <!-- /.col-md-6 -->
                                            <div class="col-md-6">
                                                
                                            </div>
                                            <!-- /.col-md-6 -->
                                        </div>
                                        <!-- /.row -->
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-3 col-md-9">
                                	<div class="row">
                                        <div class="col-md-6">
                                    		<input class="btn btn-default btn-rounded" type="submit" value="register">
                                		</div>
                                	</div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            
                        </div>
                    </form>
			    </div>
			  </div>
			</div>
		</div><!--end #content-index-->
	<?php endif ?>

		
		<div class="height20"></div>
		
		<!-- <div class="colourfull-row"></div> -->
		<div class="solid-row"></div>
		<div class="modal fade" id="forgot-modal" tabindex="-1" role="basic" aria-hidden="true">
	        <div class="modal-dialog">
	            <div class="modal-content">
	                <div class="modal-body">
	                	<div class="footer-widget">
								
							<h4>Lupa<span> Kata Sandi</span></h4>
							
							<div class="newsletter clearfix">
							
								<i class="fa fa-envelope"></i>
								<p class="newsletter-text">Masukkan alamat e-mail Anda di bawah ini untuk mereset kata sandi Anda.</p>
							
							<div class="success" id="message-news" style="display:none;">Thank you! Kami telah mengirimkan email untuk me</div>
							<form name="newsletter_form" id="newsletter_form" method="post" action="submit.php" onsubmit="return false">
								<input type="email" name="news_email_address" id="news_email_address" placeholder="Email Address" onkeypress="removeChecks();">
								<input type="submit" value="simpan" class="btn btn-default btn-rounded" onclick="validateNewsletter();">
							</form>
							
							</div>
						</div>
	                </div>
	            </div>
	            <!-- /.modal-content -->
	        </div>
	        <!-- /.modal-dialog -->
	    </div>
<?php
get_footer();