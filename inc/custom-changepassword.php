<?php
/*
Template Name: Custom change password
*/

global $wpdb, $user_ID, $result;
function handle_form_submission() {
	// do your validation, nonce and stuffs here

	// Instantiate the WP_Error object
	$my_error = new WP_Error();


	$current_user = wp_get_current_user();

	$current_password = $_POST['contact-current_password'];
	$new_password = $_POST['contact-new_password'];

	require_once ABSPATH . 'wp-includes/class-phpass.php';
	$wp_hasher = new PasswordHash( 8, true );

	$user_pass = $current_user->user_pass;

	// Check password is valid
	if(0 === preg_match("/.{6,}/", $_POST['contact-new_password'])){
		$my_error->add( 'invalid', "Password must be at least six characters." );
	}

	// Check password confirmation_matches
	if(0 !== strcmp($_POST['contact-new_password'], $_POST['contact-password_confirmation'])){
	  	$my_error->add( 'invalid', "First new password does not match with password confirmation." );	
	}

	if(!$wp_hasher->CheckPassword($current_password, $user_pass)) {
		$my_error->add( 'invalid', "Current password does not match with our data." );
	}

	// Send the result
	if ( !empty( $my_error->get_error_codes() ) ) {
		return $my_error;
	}

	// Everything is fine
	return true;
}

if( $_SERVER['REQUEST_METHOD'] == 'POST' ) {

	$current_user = wp_get_current_user();
	$new_password = $_POST['contact-new_password'];

	$result = handle_form_submission();
	
	if ( !is_wp_error( $result ) ) {
	    // Change password.
		wp_set_password($new_password, $current_user->ID);

		// Log-in again.
		wp_set_auth_cookie($current_user->ID);
		wp_set_current_user($current_user->ID);
		do_action('wp_login', $current_user->user_login, $current_user);
		$page  = get_permalink( get_page_by_title('change-password') );
		wp_redirect( $page.'/?success=1' );
	} else {
	    //Password not match 
	 //    $page  = get_permalink( get_page_by_title('change-password') );
		// wp_redirect( $page.'/?fail=1' );
	}

}
get_header(); ?>
<div class="clearfix"></div>
<div class="height20"></div>
    
	<!-- Sub Page Banner
		============================================= -->
	<!-- <section class="sub-page-banner text-center" data-stellar-background-ratio="0.3">
		
		<div class="overlay"></div>
		
		<div class="container">
			<h1 class="entry-title">Change password</h1>
		</div>
		
	</section>
 -->
	<div id="sub-page-content" class="clearfix" style="margin-top: 80px">
		<section class="team-description">
			<div class="container">
				<div class="container text-center">
					<h3 class="">Change password</h3>
				</div>

				<?php $page_showing = basename($_SERVER['REQUEST_URI']); ?>
				<?php if (strpos($page_showing, 'success') !== false) : ?>
					<div class="col-md-offset-3 col-md-6 alert alert-success" style="padding-left: 25px">
						<p class="text-success"><strong>Success:</strong> Change password success.</p>
					</div>
					<?php endif ?>
	            <?php if ( is_wp_error( $result ) ) : ?>
                <div class="form-group">
                	<div class="col-md-offset-3 col-md-6 alert alert-danger" style="padding-left: 25px">
						<p class="text-danger">Ops, something went wrong. Please rectify this errors</p>
						<ul>
							<?php echo '<li>' . implode( '</li><li>', $result->get_error_messages() ) . '</li>'; ?>
						</ul>
					</div>
                </div>
				<?php endif ?>

				<ul id="meet-specialist-carousel" class="list-unstyled owl-carousel row">
					<li>
						<div class="col-md-12">
							<div class="col-md-3"></div>
							<div class="col-md-6">
								<form action="<?php the_permalink() ?>" method="POST">
								<div class="form-body">
									<div class="form-group">
										<label class="col-md-4">Current Password</label>
										<div class="col-md-8">
											<input type="password" name="contact-current_password" class="form-control" value="">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4">New Password</label>
										<div class="col-md-8">
											<input type="password" name="contact-new_password" class="form-control" required="" value=""  minlength="6">
										</div>
									</div>
									<div class="form-group">
										<label class="col-md-4">Re-type Password</label>
										<div class="col-md-8">
											<input type="password" name="contact-password_confirmation" class="form-control" required="" value="" minlength="6">
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-offset-4 col-md-8">
											<button class="btn btn-default" type="submit">Ubah Password</button>
										</div>
									</div>
								</div>
							</form>
							</div>
							<!-- <div class="col-md-2"></div> -->
							
						</div>
					</li>
				</ul>
			</div>
		</section>
	</div><!--end sub-page-content-->
<?php
get_footer();
