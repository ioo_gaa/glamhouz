<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package GZ
 */

get_header(); ?>

    <!-- Sub Page Banner
            ============================================= -->
            <section class="sub-page-banner text-center" data-stellar-background-ratio="0.3">
                
                <div class="overlay"></div>
                
                <div class="container">
                    <h1 class="entry-title">about us</h1>
                </div>
                
            </section>
    
            
            
            <!-- Sub Page Content
            ============================================= -->
            <div id="sub-page-content">
        
        
            <div class="container big-font">
                
                <h2 class="light bordered main-title">who <span>we are</span></h2>
            
                <div class="row">
                    
                    
                    <div class="col-md-7">
                    
                        <p>Glamhouz is the important member of the MyClinicalPro Group. Established in 2016, MyClinicalPro Group has become the leading clinic management provider in Indonesia and is headquartered in Jakarta. Glamhouz provides information service for patient and will build a direct network from patient to clinics registered to MyClinicalPro. Glamhouz aims to form three major product lines around medical network, drug network, and information network, involving many areas of medical health such as online medical consultation, doctor-patient management, pharmaceutical O2O, and electronic health records. Glamhouz aims to be a leader in smart medical care in Indonesia mobile internet era and is committed to build a one-stop solution provider for patient health management and to support to develop healthcare digital ecosystems in Indonesia..</p>
                        <br><br>
                    </div>
                    
                    
                    <div class="col-md-5">
                        
                        <div class="media">
                        
                            <div class="owl-carousel image-carousel">
                            
                                <div class="item">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/about-slider-img1.jpg" width="474" class="img-rounded" alt="" title="">
                                </div>
                                
                                <div class="item">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/about-slider-img2.jpg" width="474" class="img-rounded" alt="" title="">
                                </div>
                                
                                <div class="item">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/about-slider-img3.jpg" width="474" class="img-rounded" alt="" title="">
                                </div>
                                
                            </div>
                        
                        </div>
                        
                    </div>
                
                
                </div>
                
            
            </div>
            
            
            <div class="height20"></div>
            
        </div><!--end sub-page-content-->
    
    
  
        <div class="colourfull-row"></div>
        

<?php
// get_sidebar();
get_footer();
