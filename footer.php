<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package GZ
 */

?>

		</div><!-- #document wrapper -->
	</div><!-- #content -->

	<div class="modal fade" id="subscribe-modal" tabindex="-1" role="basic" aria-hidden="true">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-body">
	            	<div class="footer-widget">
							
						<h4><span>newsletter</span></h4>
						
						<div class="newsletter clearfix">
						
							<i class="fa fa-envelope"></i>
							<p class="newsletter-text">Subscribe with your email to 	get updates fresh updates.</p>
						<?php es_subbox( $namefield = "YES", $desc = "", $group = "" ); ?>
						<div class="success" id="message-news" style="display:none;">Thank you! You have successfully subscribed.</div>
						<!-- <form name="newsletter_form" id="newsletter_form" method="post" action="submit.php" onsubmit="return false">
							<input type="email" name="news_email_address" id="news_email_address" placeholder="Email Address" onkeypress="removeChecks();">
							<input type="submit" value="subscribe" class="btn btn-default btn-rounded" onclick="validateNewsletter();"> -->
							<!-- <button type="button" class="btn btn-default btn-outline" data-dismiss="modal">Close</button> -->
						<!-- </form> -->
						
						</div>
					</div>
	            </div>
	        </div>
	        <!-- /.modal-content -->
	    </div>
	    <!-- /.modal-dialog -->
	</div>
	<!-- <footer id="colophon" class="site-footer">
		<div class="site-info">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'gz' ) ); ?>"><?php
				/* translators: %s: CMS name, i.e. WordPress. */
				printf( esc_html__( 'Proudly powered by %s', 'gz' ), 'WordPress' );
			?></a>
			<span class="sep"> | </span>
			<?php
				/* translators: 1: Theme name, 2: Theme author. */
				printf( esc_html__( 'Theme: %1$s by %2$s.', 'gz' ), 'gz', '<a href="http://underscores.me/">Underscores.me</a>' );
			?>
		</div> -->
		<!-- .site-info
	<!-- </footer> -->
	<!-- #colophon -->
	<footer id="footer" class="light">
		<div class="container">
        	
			<div class="row">
			
            	<div class="col-md-3">
                    
					<!-- Footer Widget
					============================================= -->
					<div class="footer-widget">
                        
						<h4><span>OUR SERVICES</span></h4>
                        
						<ul class="footer-nav list-unstyled clearfix">
                            <li style="float: none"><a href="#.">Practice Management</a></li>
                            <li style="float: none"><a href="#.">Features</a></li>
                            <li style="float: none"><a href="#.">Why MyClinicalPro</a></li>
                            <li style="float: none"><a href="#.">Data Analytic Services</a></li>
                            <li style="float: none"><a href="#.">Patient Portal</a></li>
                        </ul>
						
                    </div>
					
                </div>
				
            	<div class="col-md-3">
				
					<!-- Footer Widget
					============================================= -->
                	<div class="footer-widget">
                        
						<h4><span>CUSTOMER STORIES</span></h4>
						
					</div>
					
                </div>
				
            	<div class="col-md-3">
				
					<!-- Footer Widget
					============================================= -->
					<div class="footer-widget">
						
						<h4><span>COMPANY</span></h4>
						
						<ul class="footer-nav list-unstyled clearfix">
                            <li style="float: none"><a href="#.">About Us</a></li>
                            <li style="float: none"><a href="#.">Our Team</a></li>
                            <li style="float: none"><a href="#.">Contact Us</a></li>
                            <li style="float: none"><a href="#.">Career</a></li>
                            <li style="float: none"><a href="#.">Resources</a></li>
                        </ul>
						
                    </div>
					
                </div>
				
            	<div class="col-md-3">
                   
					<!-- Footer Widget
					============================================= -->
					<div class="footer-widget">
                    
						<h4><span>ASKGLOW</span></h4>
					
	                </div>
	            </div>
	        </div>
		</footer>

		<footer id="footer" class="light" style="background-color: black; color: white ">

			<!-- Footer Bottom
			============================================= -->
			<div class="container">
				<div class="row">
					<div class="footer-widget">
						
						<h4 style="border-bottom: none; color: white">Our Address</h4>
						
						<div class="contact-widget">
							<i class="fa fa-map-marker"></i><p>Union Space, PIK Avenue Lantai 6, <br>Jalan Pantai Indah Kapuk Boulevard, <br>Penjaringan, Jakarta Utara, <br>Indonesia, 14460</p>
							<i class="fa fa-phone"></i><p class="phone-number">0811 5406 909</p>
							<i class="fa fa-envelope"></i><p>sales@myclinicalpro.com</p>
						</div>
						
						<!-- Copyright
						============================================= -->
						<p class="">AskGlow Powered By <a target="_blank" href="https://myclinicalpro.com">MyClinicalPro</a> | &copy; 2018 All right reserved.</p>
					</div>

				</div>
			</div>
			<!-- back to top -->
			<a href="#." class="back-to-top" id="back-to-top"><i class="fa fa-angle-up"></i></a>
		</footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
